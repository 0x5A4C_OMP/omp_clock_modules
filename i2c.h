#ifndef I2C
#define I2C

void init_i2c();
void process_i2c();
void close_i2c();

#endif
