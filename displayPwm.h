#ifndef DISPLAY_PWM
#define DISPLAY_PWM

void init_displayPwm();
void process_displayPwm();
void close_displayPwm();

#endif
