#ifndef DISPLAY_HEARTBEAT
#define DISPLAY_HEARTBEAT

void init_heartBeat();
void process_heartBeat();
void close_heartBeat();

#endif
