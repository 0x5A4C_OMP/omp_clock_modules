#ifndef LOG
#define LOG

#include <ArduinoSimpleLogging.h>

void init_log();
void process_log();
void close_log();

#endif
