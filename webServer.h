#ifndef WEB_SERVER
#define WEB_SERVER

void init_webServer();
void process_webServer();
void close_webServer();

#endif
