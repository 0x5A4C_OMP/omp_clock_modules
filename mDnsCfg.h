#ifndef MDNSCFG_MODULE
#define MDNSCFG_MODULE

#include <Arduino.h>

#include <tuple>

std::pair<String, String> getName();
std::pair<String, String> getIp();

#endif
