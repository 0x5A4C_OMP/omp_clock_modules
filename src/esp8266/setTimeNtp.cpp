#include <mod/setTimeNtp.h>
#include <mod/log.h>

const char *ssid_l = "zxc";
const char *password_l = "1234!!!!";

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include <NTPClient.h>
#include <Time.h>
#include <TimeLib.h>
#include <Timezone.h>

#include <RTClib.h>
#include <Ticker.h>
#include <CronAlarms.h>

extern RTC_DS3231 rtc;

// Define NTP properties
#define NTP_OFFSET 60 * 60			  // In seconds
#define NTP_INTERVAL 60 * 1000		  // In miliseconds
#define NTP_ADDRESS "pl.pool.ntp.org" // change this to whatever pool is closest (see ntp.org)

// Set up the NTP UDP client
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

String date;
String t;
const char *days[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};
const char *ampm[] = {"AM", "PM"};

Ticker timer_updateSmtp;
void updateSmtp_setTimeNtp();

void setLocalTimeAsString(String h, String m, String s)
{
	auto local = now();
	setTime(h.toInt(), m.toInt(), s.toInt(), day(local), month(local), year(local));
}

String getLocalTimeAsString()
{
	String date;
	String t;
	auto local = now();

	// now format the Time variables into strings with proper names for month, day etc
	date += days[weekday(local) - 1];
	date += ", ";
	date += months[month(local) - 1];
	date += " ";
	date += day(local);
	date += ", ";
	date += year(local);

	// format the time to 12-hour format with AM/PM and no seconds
	t += hourFormat12(local);
	t += ":";
	if (minute(local) < 10) // add a zero if minute is under 10
		t += "0";
	t += minute(local);
	t += " ";
	t += ampm[isPM(local)];

	String str;
	str = date + " " + t;
	return str;
}

void displayTime_setTimeNtp(time_t local)
{
	// now format the Time variables into strings with proper names for month, day etc
	date += days[weekday(local) - 1];
	date += ", ";
	date += months[month(local) - 1];
	date += " ";
	date += day(local);
	date += ", ";
	date += year(local);

	// format the time to 12-hour format with AM/PM and no seconds
	t += hourFormat12(local);
	t += ":";
	if (minute(local) < 10) // add a zero if minute is under 10
		t += "0";
	t += minute(local);
	t += " ";
	t += ampm[isPM(local)];

	// Display the date and time
	Logger.debug.println("");
	Logger.debug.print("Local date: ");
	Logger.debug.print(date);
	Logger.debug.println("");
	Logger.debug.print("Local time: ");
	Logger.debug.print(t);
}

void init_setTimeNtp()
{
	Logger.debug.println("module: init");

	timeClient.begin(); // Start the NTP UDP client

	updateSmtp_setTimeNtp();

	// timer_updateSmtp.attach(60 * 30, updateSmtp_setTimeNtp);
	// timer_updateSmtp.attach(5, updateSmtp_setTimeNtp);

	Cron.create("0 0 * * * *", updateSmtp_setTimeNtp, false); // 8:30am every day
}

void updateSmtp_setTimeNtp()
{
	if (WiFi.status() != WL_CONNECTED) //Check WiFi connection status
	{
		WiFi.begin(ssid_l, password_l);
		while (WiFi.status() != WL_CONNECTED)
		{
			delay(500);
			Logger.debug.print(".");
		}
		Logger.debug.println("");
		Logger.debug.print("Connected to WiFi at ");
		Logger.debug.print(WiFi.localIP());
		Logger.debug.println("");
	}

	date = ""; // clear the variables
	t = "";

	// update the NTP client and get the UNIX UTC timestamp
	if (timeClient.update())
	{
		unsigned long epochTime = timeClient.getEpochTime();

		// convert received time stamp to time_t object
		time_t local, utc;
		utc = epochTime;

		TimeChangeRule CEST = {"CEST", Last, Sun, Mar, 2, 60}; //Central European Summer Time
		TimeChangeRule CET = {"CET ", Last, Sun, Oct, 3, 0};   //Central European Standard Time
		Timezone ce(CEST, CET);

		local = ce.toLocal(utc);
		setTime(local);
		rtc.adjust(local);

		displayTime_setTimeNtp(local);
	}
	else
	{
		Logger.debug.println("NTP time uptade failed.");
	}
}

void process_setTimeNtp()
{
	// Logger.debug.println("module: process");

	// Cron.delay(1000);
}

void close_setTimeNtp()
{
	Logger.debug.println("module: close");
}
