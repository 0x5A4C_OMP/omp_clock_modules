#include <mod/setTimeRtc.h>
#include <mod/log.h>

#include <Time.h>
#include <RTClib.h>

#include <CronAlarms.h>

RTC_DS3231 rtc;

String getAsString_RtcTime()
{
	auto now = rtc.now();
	String str = String(now.year(), DEC) + '/' + String(now.month(), DEC) + '/' + String(now.day(), DEC) + " " + String(now.hour(), DEC) + ':' + String(now.minute(), DEC) + ':' + String(now.second(), DEC);
	return str;
}

void displayRtcTime(DateTime now)
{
	const char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

	Logger.debug.print(now.year(), DEC);
	Logger.debug.print('/');
	Logger.debug.print(now.month(), DEC);
	Logger.debug.print('/');
	Logger.debug.print(now.day(), DEC);
	Logger.debug.print(" (");
	Logger.debug.print(daysOfTheWeek[now.dayOfTheWeek()]);
	Logger.debug.print(") ");
	Logger.debug.print(now.hour(), DEC);
	Logger.debug.print(':');
	Logger.debug.print(now.minute(), DEC);
	Logger.debug.print(':');
	Logger.debug.print(now.second(), DEC);
	Logger.debug.println();

	Logger.debug.print(" since midnight 1/1/1970 = ");
	Logger.debug.print(now.unixtime());
	Logger.debug.print("s = ");
	Logger.debug.print(now.unixtime() / 86400L);
	Logger.debug.println("d");

	// calculate a date which is 7 days and 30 seconds into the future
	DateTime future(now + TimeSpan(7, 12, 30, 6));

	Logger.debug.print(" now + 7d + 30s: ");
	Logger.debug.print(future.year(), DEC);
	Logger.debug.print('/');
	Logger.debug.print(future.month(), DEC);
	Logger.debug.print('/');
	Logger.debug.print(future.day(), DEC);
	Logger.debug.print(' ');
	Logger.debug.print(future.hour(), DEC);
	Logger.debug.print(':');
	Logger.debug.print(future.minute(), DEC);
	Logger.debug.print(':');
	Logger.debug.print(future.second(), DEC);
	Logger.debug.println();

	Logger.debug.println();
}

long int getRtcSynchTime()
{
	displayRtcTime(rtc.now());
	return rtc.now().unixtime();
}

void setLocalTime()
{
	displayRtcTime(rtc.now());
	setTime(rtc.now().unixtime());
}

void init_setTimeRtc()
{
	Logger.debug.println("set time rtc: init");

	if (!rtc.begin())
	{
		Logger.error.println("Couldn't find RTC");
		return;
	}

	if (rtc.lostPower() || (rtc.now().year() <= 2000))
	{
		Logger.error.println("RTC lost power, lets set the time!");
		rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
	}

	// setSyncProvider(getRtcSynchTime);
	// if (timeStatus() != timeSet)
	// {
	//      Logger.error.println("set time rtc: fail");
	// }
	// setSyncInterval(600);

	Cron.create("0 15 * * * *", setLocalTime, false);
	Cron.create("0 30 * * * *", setLocalTime, false);
	Cron.create("0 45 * * * *", setLocalTime, false);

	Logger.debug.println("set time rtc: init end");
}

void process_setTimeRtc()
{
#if 0
	Logger.debug.println("set time rtc: process");

	displayRtcTime(rtc.now());

	delay(3000);
#endif

	// Cron.delay(1000);
}

void close_setTimeRtc()
{
	Logger.debug.println("set time rtc: close");
}
