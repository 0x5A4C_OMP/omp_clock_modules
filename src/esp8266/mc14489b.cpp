#include <mod/mc14489b.h>
#include <mod/log.h>

#include <Arduino.h>

// Vss      o   o Vss
//          o   o
// clock    o   o
// enable   o   o data in / data out (on second connector)
// Vdd (+5) o   o Vdd (+5)

Mc14489b::Mc14489b(int data, int clock, int enable)
{
	pinMode(data, OUTPUT);
	pinMode(enable, OUTPUT);
	pinMode(clock, OUTPUT);
	_data = data;
	_enable = enable;
	_clock = clock;

	byte2 = 0;
	byte1 = 0;
	byte0 = 0;
}

void Mc14489b::test()
{
	byte2 = B00000101;
	byte1 = B01000011;
	byte0 = B00100001;
}

void Mc14489b::update()
{
	digitalWrite(_enable, LOW);
	shiftOut(_data, _clock,  MSBFIRST, byte2);
	shiftOut(_data, _clock,  MSBFIRST, byte1);
	shiftOut(_data, _clock,  MSBFIRST, byte0);
	digitalWrite(_enable, HIGH);
	delay(10);
}

void Mc14489b::control(uint8_t val)
{
	digitalWrite(_enable, LOW);
	shiftOut(_data, _clock,  MSBFIRST, val);
	digitalWrite(_enable, HIGH);
	delay(10);
}

void Mc14489b::displayOn()
// turns on display
{
	digitalWrite(_enable, LOW);
	shiftOut(_data, _clock,  MSBFIRST, B00000001);
	digitalWrite(_enable, HIGH);
	delay(10);
}

void Mc14489b::displayOff()
// turns off display
{
	digitalWrite(_enable, LOW);
	shiftOut(_data, _clock,  MSBFIRST, B00000000);
	digitalWrite(_enable, HIGH);
	delay(10);
}

void Mc14489b::digit0(uint8_t val)
{
	byte0 &= B11110000;
	byte0 |= val;
}

void Mc14489b::digit1(uint8_t val)
{
	byte0 &= B00001111;
	byte0 |= (val << 4);
}

void Mc14489b::digit2(uint8_t val)
{
	byte1 &= B11110000;
	byte1 |= val;
}

void Mc14489b::digit3(uint8_t val)
{
	byte1 &= B00001111;
	byte1 |= (val << 4);
}

void Mc14489b::digit4(uint8_t val)
{
	byte2 &= B11110000;
	byte2 |= val;
}

void Mc14489b::setDot(uint8_t idx)
{
	byte2 &= B00001111;
	byte2 |= (idx << 4);
}
