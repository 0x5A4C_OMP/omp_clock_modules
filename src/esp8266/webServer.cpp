#include <mod/webServer.h>
#include <mod/log.h>
#include <mod/index.h>
#include <mod/getTempPress.h>

#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>


ESP8266WebServer server;

void handleRoot()
{
    Logger.debug.println("webServer: handle root");

    String s = MAIN_page;
    server.send(200, "text/html", s);
}

float humidity, temperature;

void handleData()
{
    // int a = analogRead(A0);

    // String data = "{\"ADC\":\""+String(a)+"\", \"Temperature\":\""+ String(temperature) +"\", \"Humidity\":\""+ String(humidity) +"\"}";

    server.send(200, "text/html", "Hello World!"); //Send ADC value, temperature and humidity JSON to client ajax request
    Serial.println("web server");
    // humidity = press_getTempPress();
    // temperature = temp_getTempPress();
}

void init_webServer()
{
    Serial.println("-----------");
    Logger.debug.println("webServer: init");

    // server.on("/", handleData);
    // server.on("/readADC", handleData);

    server.on("/", []() { server.send(200, "text/plain", "Hello World!"); Serial.println("web server"); });
    // server.on("/", []() { server.send(200, "text/plain", "Temperature:" + String(temp_getTempPress()) + "\n" + "Press:" + String(press_getTempPress()) + "\n" + "Alt: " + String(alt_getTempPress())); });

    server.begin();
}

void process_webServer()
{
    // Logger.debug.println("webServer: process");

    server.handleClient();
}

void close_webServer()
{
    // Logger.debug.println("webServer: close");
}
