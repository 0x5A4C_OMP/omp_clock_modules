#include <mod/mod.h>

#include <Arduino.h>

#include "mod/blynkFeed.h"
// #include "mod/displayMc14489b.h"
#include "mod/displayPwm.h"
#include "mod/displaySer.h"
#include "mod/getTempPress.h"
#include "mod/heartBeat.h"
#include "mod/i2c.h"
#include "mod/log.h"
#include "mod/pcMonData.h"
#include "mod/pcMonGfx.h"
#include "mod/pcMonPresenter.h"
#include "mod/setTimeNtp.h"
#include "mod/setTimeRtc.h"
#include "mod/setUpWiFi.h"
// #include "mod/configWiFi.h"
#include "mod/webServer.h"
#include "mod/mDns.h"

namespace Mod
{
void modInit()
{
    init_log();
    // init_i2c();
    // init_setTimeRtc();
    init_setUpWiFi();
    // init_configWiFi();
    // init_heartBeat();
    // init_displayPwm();
    // init_dispSer();
    init_setTimeNtp();
    init_webServer();
    // init_getTempPress();
    // init_blynkFeed();
    // init_displayMc14489b();
    // init_pcMonGfx();
    // init_pcMonPresenter();
    // init_pcMonData();
    init_mDns();
}

void modLoop()
{
    // process_i2c();
    // process_setTimeRtc();
    // process_heartBeat();
    // process_dispSer();
    process_setUpWiFi();
    // process_configWiFi();
    process_setTimeNtp();
    process_webServer();
    // process_getTempPress();
    // process_blynkFeed();
    // process_displayMc14489b();
    // process_pcMonGfx();
    // process_pcMonPresenter();
    // process_pcMonData();
    process_mDns();
}
}