#include <mod/pcMonData.h>
#include <mod/log.h>

#include <functional>
#include <map>

std::map<const char *, int> defaultData;
std::map<const char *, std::map<const char *, int>> set = {
    {"default", defaultData}};

void setCpuUsage(String strValue, const char *setId = "default") {}
void setCpuFan(String strValue, const char *setId = "default") {}
void setCpuTemp(String strValue, const char *setId = "default") {}
void setGpuTemp(String strValue, const char *setId = "default") {}
void setGpuFan(String strValue, const char *setId = "default") {}
void setRamUsage(String strValue, const char *setId = "default") {}
void setDiskUsage(String strValue, const char *setId = "default") {}
void setCaseTemp(String strValue, const char *setId = "default") {}
void setCaseFan(String strValue, const char *setId = "default") {}
void setEthDown(String strValue, const char *setId = "default") {}
void setEthUp(String strValue, const char *setId = "default") {}
void setTime(String strValue, const char *setId = "default") {}

void findStoreData(String buffer, String command)
{
  if (buffer.indexOf(command) == 0)
  {
    String s = buffer.substring(16);
    // Serial.print("OK\n");

    set["default"][command.c_str()] = s.toInt();
  }
}

void getPcData(String buffer)
{
  findStoreData(buffer, "cpu_usage");
  findStoreData(buffer, "cpu_temp");
  findStoreData(buffer, "cpu_fan");

  findStoreData(buffer, "gpu_temp");
  findStoreData(buffer, "gpu_fan");

  findStoreData(buffer, "mem_ram");
  findStoreData(buffer, "mem_hdd");

  findStoreData(buffer, "case_temp");
  findStoreData(buffer, "case_fan");

  findStoreData(buffer, "net_in");
  findStoreData(buffer, "net_out");

  findStoreData(buffer, "time");
}

void init_pcMonData() { Logger.debug.println("pcMonData: init"); }

void process_pcMonData()
{
  Logger.debug.println("pcMonData: process");

  // getPcData();
}

void close_pcMonData() { Logger.debug.println("pcMonData: close"); }
