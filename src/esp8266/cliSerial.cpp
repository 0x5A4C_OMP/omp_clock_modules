#include <mod/cliSerial.h>
#include <mod/cli.h>

#include <Arduino.h>

Cli cliSerial = Cli(Serial);

void init_cliSerial()
{
    cliSerial.init();
}

void process_cliSerial()
{
    cliSerial.process();
}

void close_cliSerial()
{
}