#ifndef MC14489B
#define MC14489B

#include <stdint.h>

class Mc14489b
{
public:
  Mc14489b(int data, int clock, int enable);
  void test();
  void update();
  void displayOn();
  void displayOff();
  void digit0(uint8_t val);
  void digit1(uint8_t val);
  void digit2(uint8_t val);
  void digit3(uint8_t val);
  void digit4(uint8_t val);
  void digit5(uint8_t val);
  void control(uint8_t val);
  void setDot(uint8_t idx);

private:
  int _data;
  int _clock;
  int _enable;

  uint8_t byte0;
  uint8_t byte1;
  uint8_t byte2;
};

#endif
