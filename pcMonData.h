#ifndef PCMON_DATA_MODULE
#define PCMON_DATA_MODULE

void init_pcMonData();
void process_pcMonData();
void close_pcMonData();

#endif
